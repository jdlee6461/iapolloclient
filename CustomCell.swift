//
//  CustomCell.swift
//  InfoMaxApollo
//
//  Created by Shreya Garg on 4/19/15.
//  Copyright (c) 2015 akash_kapoor. All rights reserved.
//

import UIKit
import SwiftNDN
import QuartzCore

class CustomCell: UITableViewCell {

    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var tweet: UITextView!
    @IBOutlet weak var storyImage: UIImageView!

    var imageCache = [String : UIImage]()
    var prefix: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(tweet: Tweet) {
        self.time.text = tweet.time
        self.tweet.text = tweet.tweet+" "+tweet.EMScore+" "+tweet.clusterSize
        
        if (tweet.haveSimilarTweets == true) {
            self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        } else {
            self.accessoryType = UITableViewCellAccessoryType.None
        }
        
        let imageUrl = tweet.imageName
        if (imageUrl != "")
        {
            downloadImage(imageUrl, completionHandler: { (result, image) -> Void in
                self.imageCache[imageUrl] = image
                dispatch_async(dispatch_get_main_queue(), {
                    self.storyImage.image = image
                })
            })
        }
    }
   
//    func setCell(userName: String, tweet: String, time: String, imageName: String){
//        
//        self.time.text = time
//        self.tweet.text = tweet
//        
//        let imageUrl = imageName
//        if (imageUrl != "")
//        {
//            downloadImage(imageUrl, completionHandler: { (result, image) -> Void in
//            self.imageCache[imageUrl] = image
//            dispatch_async(dispatch_get_main_queue(), {
//                self.storyImage.image = image
//                })
//            })
//        }
    
        //self.username.text =  userName
        
//    }
}
