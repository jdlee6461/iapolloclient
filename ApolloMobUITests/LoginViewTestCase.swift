//
//  LoginViewTestCase.swift
//  ApolloMob
//
//  Created by panindra Ts on 9/21/15.
//  Copyright © 2015 panindra Ts. All rights reserved.
//

import XCTest

class LoginViewTestCase: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBasicLogin() {
        let app = XCUIApplication()
        app.scrollViews.otherElements.icons["ApolloMob"].tap()
        app.textFields["Username"].typeText("admin")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("apollo")
        app.buttons["Login"].tap()
    }
    
    func testGoogleLogin() {
        
        let app = XCUIApplication()
        app.buttons["GIDSignInButton"].tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("apollouiuc")
        
        let passwordTextField = app.secureTextFields["Password"]
        passwordTextField.tap()
        passwordTextField.typeText("Apollo2015")
        app.buttons["Sign in"].tap()
        let allowButton = app.buttons["Allow"]
        allowButton.pressForDuration(0.8);
        allowButton.tap()
        
        
    }
    
    func testFacebookLogin() {
        XCUIApplication().buttons["Log in with Facebook"].tap()
    }
    
    func testCreateTask() {
    
        let app = XCUIApplication()
        app.buttons["Log in with Facebook"].tap()
        app.navigationBars["My Tasks"].buttons["Add"].tap()
    
        let element = app.childrenMatchingType(.Window).elementBoundByIndex(0).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element
        let textField = element.childrenMatchingType(.TextField).elementBoundByIndex(0)
        textField.tap()
        textField.typeText("sample")
        
        let textField2 = element.childrenMatchingType(.TextField).elementBoundByIndex(1)
        textField2.tap()
        textField2.typeText("sample")
        
        let textField3 = element.childrenMatchingType(.TextField).elementBoundByIndex(2)
        textField3.tap()
        textField3.typeText("sample")
        
        let textField4 = element.childrenMatchingType(.TextField).elementBoundByIndex(3)
        textField4.tap()
        textField4.typeText("sample")
        app.buttons["Create Task"].tap()
    
    }
    
    func testDeleteTask() {
        
        let app = XCUIApplication()
        app.buttons["GIDSignInButton"].tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("apollouiuc")
        
        let passwordTextField = app.secureTextFields["Password"]
        passwordTextField.tap()
        passwordTextField.typeText("Apollo2015")
        app.buttons["Sign in"].tap()
        
        let allowButton = app.buttons["Allow"]
        allowButton.pressForDuration(0.8);
        allowButton.tap()
        
        app.tables.staticTexts["nfl1"].pressForDuration(1.1);
        app.sheets["Task Action"].collectionViews.buttons["Delete"].tap()
        app.alerts["nfl1"].collectionViews.buttons["OK"].tap()
        
    }
    
    func testPauseTask() {
        
        
        let app = XCUIApplication()
        app.buttons["GIDSignInButton"].tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("apollouiuc")
        
        let passwordSecureTextField = app.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("Apollo2015")
        
        let signInButton = app.buttons["Sign in"]
        signInButton.pressForDuration(0.8);
        signInButton.tap()
        
        let allowButton = app.buttons["Allow"]
        let exists = NSPredicate(format: "exists == true")
        
        expectationForPredicate(exists, evaluatedWithObject: allowButton, handler: nil)
        waitForExpectationsWithTimeout(5, handler: nil)
        
        allowButton.pressForDuration(0.5)
        
        let table = app.tables.staticTexts["nfl"]
        
        expectationForPredicate(exists, evaluatedWithObject: table, handler: nil)
        waitForExpectationsWithTimeout(5, handler: nil)
        
        
        app.tables.staticTexts["nfl"].pressForDuration(1.1);
        app.sheets["Task Action"].collectionViews.buttons["Pause"].tap()
        app.alerts["nfl"].collectionViews.buttons["OK"].tap()
        app.navigationBars["My Tasks"].buttons["Sign out"].tap()
        
    }
    
    func testResumeTask() {
        
        let app = XCUIApplication()
        app.buttons["GIDSignInButton"].tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("apollouiuc")
        
        let passwordTextField = app.secureTextFields["Password"]
        passwordTextField.tap()
        passwordTextField.typeText("Apollo2015")
        app.buttons["Sign in"].tap()

        let exists = NSPredicate(format: "exists == true")
        
        let allowButton = app.buttons["Allow"]
        expectationForPredicate(exists, evaluatedWithObject: allowButton, handler: nil)
        waitForExpectationsWithTimeout(5, handler: nil)
        
        allowButton.pressForDuration(0.5)
        
        let table = app.tables.staticTexts["nfl"]
        
        expectationForPredicate(exists, evaluatedWithObject: table, handler: nil)
        waitForExpectationsWithTimeout(5, handler: nil)
        
        app.tables.staticTexts["nfl"].pressForDuration(1.1);
        app.sheets["Task Action"].collectionViews.buttons["Resume"].tap()
        app.alerts["nfl"].collectionViews.buttons["OK"].tap()
        app.navigationBars["My Tasks"].buttons["Sign out"].tap()
    }
    
    func testViewStoryline() {
        
        let app = XCUIApplication()
        app.buttons["GIDSignInButton"].tap()
        
        let emailTextField = app.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText("apollouiuc")
        
        let passwordTextField = app.secureTextFields["Password"]
        passwordTextField.tap()
        passwordTextField.typeText("Apollo2015")
        app.buttons["Sign in"].tap()
        
        let allowButton = app.buttons["Allow"]
        let exists = NSPredicate(format: "exists == true")
        
        expectationForPredicate(exists, evaluatedWithObject: allowButton, handler: nil)
        waitForExpectationsWithTimeout(5, handler: nil)
        
        allowButton.pressForDuration(0.5)
        
        let table = app.tables.staticTexts["nfl"]
        
        expectationForPredicate(exists, evaluatedWithObject: table, handler: nil)
        waitForExpectationsWithTimeout(5, handler: nil)
        
        app.tables.staticTexts["nfl"].pressForDuration(1.1);
        
        let viewStorylineButton = app.sheets["Task Action"].collectionViews.buttons["View Storyline"]
        viewStorylineButton.tap()
    }
}
