//
//  CustomCell.swift
//  InfoMaxApollo
//
//  Created by Shreya Garg on 4/19/15.
//  Copyright (c) 2015 akash_kapoor. All rights reserved.
//

import UIKit
import QuartzCore

class CustomCell2: UITableViewCell {
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var username: UILabel!
    //    @IBOutlet weak var tweet: UILabel!
    @IBOutlet weak var tweet: UITextView!
    //    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let fixedWidth = tweet.frame.size.width
        tweet.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        let newSize = tweet.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        var newFrame = tweet.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        tweet.frame = newFrame
        tweet.scrollEnabled = false
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(tweet: Tweet){
        
        self.time.text = tweet.time
        self.tweet.text = tweet.tweet+" "+tweet.EMScore+" "+tweet.clusterSize
        
        if (tweet.haveSimilarTweets == true) {
            self.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        } else {
            self.accessoryType = UITableViewCellAccessoryType.None
        }
        
    }
}
