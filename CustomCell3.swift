//
//  CustomCell.swift
//  InfoMaxApollo
//
//  Created by Shreya Garg on 4/19/15.
//  Copyright (c) 2015 akash_kapoor. All rights reserved.
//

import UIKit
import QuartzCore

class CustomCell3: UITableViewCell {
    
    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var headLine: UILabel!
    //    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let fixedWidth = headLine.frame.size.width
        headLine.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        let newSize = headLine.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        var newFrame = headLine.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        headLine.frame = newFrame
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(taskName: String, headLine: String){
        self.taskName.text = taskName
        self.headLine.text = headLine
        
    }
}
