//
//  MapUtil.swift
//  ApolloMob
//
//  Created by panindra Ts on 9/15/15.
//  Copyright © 2015 panindra Ts. All rights reserved.
//

import Foundation
import MapKit

class MapPin : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var index : Int?
    
    init(coordinate: CLLocationCoordinate2D, title: String, index: Int) {
        self.coordinate = coordinate
        self.title = title
        self.index = index
    }
}

protocol MapViewDelegate : MKMapViewDelegate {
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
}
