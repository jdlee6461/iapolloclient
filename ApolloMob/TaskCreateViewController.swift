//
//  TaskCreateViewController.swift
//  ApolloMob
//
//  Created by panindra Ts on 9/15/15.
//  Copyright © 2015 panindra Ts. All rights reserved.
//

import UIKit
import MapKit

let CrawlerRadius = "50"
let CrawlerType = "search_api"
let QueryType = "OR"
let AnalysisTypes = ["EM_SOCIAL"]
let DefaultAnalysis = "EM_SOCIAL";
let Permission = "private";

/// Class to create a task
public class TaskCreateViewController: UIViewController, CreateRequesterDelegate, MapViewDelegate {
    var username: String?
    
    @IBOutlet weak var createTaskMapView: MKMapView!
    @IBOutlet weak var taskNameTxtField: UITextField!
    @IBOutlet weak var keyword1TxtField: UITextField!
    @IBOutlet weak var latitudeTxtField: UITextField!
    @IBOutlet weak var keyword2txtField: UITextField!
    @IBOutlet weak var longitudeTxtField: UITextField!
    @IBOutlet weak var keyword3TxtField: UITextField!
    
    var createRequester: CreateRequester!
    var prefix: String!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let latitude = 40.11415
        let longitude = -88.22528
        
        self.latitudeTxtField.text = String(latitude)
        self.longitudeTxtField.text = String(longitude)
        
        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        createTaskMapView.delegate = self
        
        let mapPin = MapPin(coordinate: location, title : "Drag Me", index : 0)
        self.createTaskMapView.addAnnotation(mapPin)
        
        openCreateRequesterConnection()
    }
    
    func openCreateRequesterConnection()
    {
        print ("CreateRequester connection")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.prefix = InterestType.createType + self.username!
            self.createRequester = CreateRequester(delegate: self, prefix: self.prefix, forwarderIP: defaultForwarderIP, forwarderPort: defaultPort)
        }
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Map view delegate
    public func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MapPin {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
                as? MKPinAnnotationView { // 2
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                // 3
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.draggable = true
            }
            return view
        }
        return nil
    }
    
    /**
     Function to create a task based on the parameters of a form
     
     - parameter sender: button
     */
    @IBAction func onCreateTask(sender: AnyObject) {
        guard let userName = self.username else {
            log.error("Username cannot be empty")
            return
        }
        if let keyword1 = keyword1TxtField.text, keyword2 = keyword2txtField.text, keyword3 = keyword3TxtField.text, taskName = taskNameTxtField.text, latitude = latitudeTxtField.text, longitude = longitudeTxtField.text {
            
            self.prefix = self.prefix + "/" + taskName + "/" + keyword1 + ":" + keyword2 + ":" + keyword3 + "/" + latitude + ":" + longitude
            
            self.createRequester.makeRequest(self.prefix)
            
            //            createTask(keyword1, keyword2: keyword2, keyword3: keyword3, Taskname: taskName, latitude: latitude, longitude: longitude, radius: CrawlerRadius, crawler: CrawlerType, created_by: userName, query_type: QueryType, analysis_types: AnalysisTypes, default_analysis: DefaultAnalysis, permission: Permission) { (result) -> Void in
            //
            //                if result?.statusCode == 200 {
            //                    self.navigationController?.popViewControllerAnimated(true)
            //                    let parentViewController = self.navigationController?.topViewController as! TasksViewController
            //                    parentViewController.tasks.removeAll()
            //                    parentViewController.openUserInfoConnection()
            //                }
            //            }
        }
    }
    
    //Mapview delegate to fill the latitude and longitude when the annotation is dragged.
    public func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, didChangeDragState newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        switch newState {
        case .Ending:
            if let annotation = view.annotation as? MapPin {
                let coordinates = annotation.coordinate
                self.latitudeTxtField.text = String(coordinates.latitude)
                self.longitudeTxtField.text = String(coordinates.longitude)
            }
            break
        default : break
        }
    }
    
    public func onOpen() {
        print("Info: CreateRequester Open")
    }
    
    public func onClose() {
        print("Info: CreateRequester Close.")
    }
    
    public func onError(reason: String) {
        print("ERROR: CreateRequester - " + reason)
    }
    
}
