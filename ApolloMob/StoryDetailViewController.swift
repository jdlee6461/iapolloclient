//
//  StoryDetailViewController.swift
//  InfoMaxApollo
//
//  Copyright (c) 2015 akash_kapoor. All rights reserved.
//

import UIKit
import SwiftNDN

class StoryDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, InfoMaxConsumerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var typeNavigator: UISegmentedControl!
    
    var arrayOfTweets:[Tweet] = [Tweet]()
    var selectedStory = ""
    var storyInterest = ""
    var consumer: InfoMaxConsumer!
    
    var prefix: String!
    var isBottomReached = false
    var isNextTweetFetched = false
    
    var listCount = 1
    var tweetCount = 0
    var selectedSegment = viewType.viewDiversity
    
    @IBAction func typeChanged(sender: UISegmentedControl) {
        if typeNavigator.selectedSegmentIndex == 0 {
//            print ("Diversity is selected")
            launchViewController(viewType.viewDiversity)
        } else if typeNavigator.selectedSegmentIndex == 1 {
//            print ("EMSocial is selected")
            launchViewController(viewType.viewSort)
        } else {
//            print ("Popular is selected")
            launchViewController(viewType.viewVoting)
        }
    }
    
    func launchViewController(type: Int) {
        let viewController : StoryDetailViewController = (UIStoryboard(name: StoryBoardIdentifiers.MainViewController, bundle: nil) .instantiateViewControllerWithIdentifier(ViewControllerIdentifiers.TaskStoryDetailViewController) as? StoryDetailViewController)!
        viewController.title = self.title
        viewController.selectedStory = self.selectedStory
        viewController.storyInterest = self.storyInterest
        viewController.selectedSegment = type
        
        // Flip animation
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = "flip"
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 250
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(title: "My Tasks", style: UIBarButtonItemStyle.Plain, target: self, action: "backButton:")
        self.navigationItem.setLeftBarButtonItem(backButton, animated: true)
        //        let nextBut = UIBarButtonItem(title: "Diversity", style: UIBarButtonItemStyle.Plain, target: self, action: "buttonAction:" )
        //        self.navigationItem.setRightBarButtonItem(nextBut, animated: true)

        self.typeNavigator.selectedSegmentIndex = self.selectedSegment
        
        // Do any additional setup after loading the view.
        UINavigationBar.appearance().backgroundColor = UIColor.lightGrayColor()
        self.automaticallyAdjustsScrollViewInsets = false
        openInfoMaxConnection()
        self.tableView.reloadData()
        
    }
    
    func openInfoMaxConnection()
    {
        print ("InfoMax connection")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.prefix = self.storyInterest
            self.consumer = InfoMaxConsumer(delegate: self, prefix: self.prefix, forwarderIP: defaultForwarderIP, forwarderPort: defaultPort)
        }
    }
    
    func backButton(sender:UIButton!)
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func buttonAction(sender:UIButton!)
    {
        //Keep for later use
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let maxPosition = scrollView.contentInset.top + scrollView.contentSize.height + scrollView.contentInset.bottom - scrollView.bounds.size.height;
        let currentPosition = scrollView.contentOffset.y + self.topLayoutGuide.length;
        
        if (currentPosition >= maxPosition) {
            isBottomReached = true
            if(isNextTweetFetched == false && isBottomReached == true) {
                isNextTweetFetched = true
                getNextTweetSet()
            }
        } else {
            isBottomReached = false
            isNextTweetFetched = false
        }
    }
    
    // MARK: InfoMaxDelegate
    
    func onData(i: Interest, d: Data) {
        
        let rawContent = d.getContent()
        let tweetText = NSString(bytes: rawContent, length: rawContent.count, encoding: NSUTF8StringEncoding)
        let tweetStr = String(tweetText!)
        
        let JSONData = tweetStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        var tweetDict = [:]
        do {
            tweetDict = try NSJSONSerialization.JSONObjectWithData(JSONData!, options: .AllowFragments) as! NSDictionary
        } catch {
            print(error)
        }
        
        if(tweetDict.count == 0) {
            return
        }
        
        let twe = Tweet(userName: " ", tweet: tweetDict.valueForKey("text") as! String, time: tweetDict.valueForKey("created_at") as! String, imageName: tweetDict.valueForKey("claim_img") as! String, suffix:i.name.toUri(), haveSimilarTweets: tweetDict.valueForKey("haveSimilarTweets") as! Bool, EMScore: tweetDict.valueForKey("EMScore") as! String, clusterSize: tweetDict.valueForKey("clusterSize") as! String)
        
        arrayOfTweets.append(twe)
        self.tweetCount = self.tweetCount+1
        if (self.tweetCount % 10 == 0) {
            self.tableView.reloadData()
        }
    }
    
    func getNextTweetSet() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if self.typeNavigator.selectedSegmentIndex == 0 {
                self.consumer.getList(self.listCount, type: InterestTagType.InfoMaxDiversityTag)
                self.listCount = self.listCount + 1
            } else if self.typeNavigator.selectedSegmentIndex == 1 {
                self.consumer.getList(self.listCount, type: InterestTagType.InfoMaxSortTag)
                self.listCount = self.listCount + 1
            } else if self.typeNavigator.selectedSegmentIndex == 2 {
                self.consumer.getList(self.listCount, type: InterestTagType.InfoMaxVotingTag)
                self.listCount = self.listCount + 1
            }
        }
    }
    
    func onOpen() {
        getNextTweetSet()
    }
    
    func onError(reason: String) {
    }
    
    func onClose() {
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTweets.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as? CustomCell
        let cell2 = tableView.dequeueReusableCellWithIdentifier("Cell2") as? CustomCell2
        
        let tweet = arrayOfTweets[indexPath.row]
        
        if (tweet.imageName != "") {
            cell!.setCell(tweet)
            return cell!
        } else {
            cell2!.setCell(tweet)
            return cell2!
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let indexPath = self.tableView.indexPathForSelectedRow()?.row
        //var selectedTweet = self.arrayOfTweets[indexPath!]
        //self.consumer.getNearestNeighbor(selectedTweet.suffix)
        //self.performSegueWithIdentifier("SuperStoryDetailSegue", sender: tableView)
        
    }

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        let indexPath = self.tableView.indexPathForSelectedRow?.row
        let selectedTweet = self.arrayOfTweets[indexPath!]
        
        if selectedTweet.haveSimilarTweets == true {
            return true
        }
        
        return false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "SuperStoryDetailSegue" || segue.identifier == "SuperStoryDetailSegue2"{
            if let destinationVC = segue.destinationViewController as? SuperStoryDetail{
                let indexPath = self.tableView.indexPathForSelectedRow?.row
                let selectedTweet = self.arrayOfTweets[indexPath!]
                print(selectedTweet.tweet)
                destinationVC.title = selectedTweet.tweet
                destinationVC.prefix = self.prefix
                destinationVC.selectedStorySuffix = selectedTweet.suffix
            }
        }
    }
}
