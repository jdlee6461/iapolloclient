//
//  TasksViewController.swift
//  ApolloMob
//
//  Created by panindra Ts on 9/12/15.
//  Copyright © 2015 panindra Ts. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
import FBSDKLoginKit
import SwiftNDN

/// Home page for the App
class TasksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MapViewDelegate, UserInfoConsumerDelegate {
    var username: String?
    var tasks : [Task] = []
    
    let forwarderIP = defaultForwarderIP
    let forwarderPort:UInt16 = defaultPort
    let prefix = Name(url: InterestType.defaultType)
    let createStoryPrefix = InterestType.createType
    var userInfoInterest = InterestType.userType
    
    var userInfoConsumer: UserInfoConsumer!
    var userDataStr = ""
    
    var dataCount = 0
    
    @IBOutlet weak var tasksTableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    var tasksTitleArray:[String] = [String]()
    
    var searchArray:[String] = [String](){
        didSet  {self.tasksTableView.reloadData()}
    }
    
    var taskSearchController = UISearchController()
    
    func openUserInfoConnection()
    {
        print ("UserInfo connection")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let prefix = self.userInfoInterest + self.username!
            self.userInfoConsumer = UserInfoConsumer(delegate: self, prefix: prefix, forwarderIP: self.forwarderIP, forwarderPort: self.forwarderPort)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        // Do any additional setup after loading the view.
        tasksTableView.dataSource = self
        tasksTableView.delegate = self
        
        /**
        Function to get the tasks and update the MapView.
        */
        openUserInfoConnection()
        
        // Long pressed gesture recognizer for a tableViewCell
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: "longPressed:")
        self.tasksTableView.addGestureRecognizer(longPressRecognizer)
        
        self.taskSearchController = ({
            // Two setups provided below:
            
            // Setup One: This setup present the results in the current view.
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.hidesNavigationBarDuringPresentation = false
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.searchBarStyle = .Minimal
            controller.searchBar.placeholder = "Search for Task Name"
            controller.searchBar.sizeToFit()
            self.tasksTableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y -= keyboardSize.height
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y += keyboardSize.height
            self.taskSearchController.active = false
        }
    }
    
    /**
     Function to launch TaskCreateViewController.
     
     - parameter sender: Button
     */
    @IBAction func onCreatetask(sender: AnyObject) {
        let viewController : TaskCreateViewController = (UIStoryboard(name: StoryBoardIdentifiers.MainViewController, bundle: nil) .instantiateViewControllerWithIdentifier(ViewControllerIdentifiers.TaskCreateViewController) as? TaskCreateViewController)!
        viewController.username = self.username
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    /**
     Function to Signout from all Networks and dismiss Login View Controller
     
     - parameter sender: Button
     */
    @IBAction func onSignOut(sender: AnyObject) {
        //        GIDSignIn.sharedInstance().signOut()
        //        FBSDKLoginManager().logOut()
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    /**
     Function that executes on Long presses gesture recognizer
     
     - parameter gestureRecognizer: gesture recongnizer object
     */
    func longPressed(gestureRecognizer : UIGestureRecognizer) {
        let location = gestureRecognizer.locationInView(self.tasksTableView)
        let indexPath = self.tasksTableView.indexPathForRowAtPoint(location)
        
        guard let selectedRow = indexPath?.row else {
            log.error("Unrecognized row")
            return
        }
        
    }
    
    // TableView delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.taskSearchController.active)
        {
            return self.searchArray.count
        } else
        {
            return self.tasksTitleArray.count
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell3") as! CustomCell3
        
        let taskName = self.tasksTitleArray[indexPath.row]
        var commonWords = ""
        
        if ((tasks[indexPath.row].taskMostCommonWords?.isEmpty) != nil) {
            let words = tasks[indexPath.row].taskMostCommonWords!
            commonWords = words.joinWithSeparator(", ")
        } else {
            commonWords = "Task empty"
        }
        
        
        cell.taskName.textColor = giveTextColor(tasks[indexPath.row].taskStatus!)
        
        cell.setCell(taskName, headLine: commonWords)
        
        
        if (self.taskSearchController.active)
        {
            cell.taskName.text! = self.searchArray[indexPath.row]
            return cell
        } else {
            cell.taskName.text = self.tasksTitleArray[indexPath.row]
            cell.taskName.textColor = giveTextColor(tasks[indexPath.row].taskStatus!)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (self.taskSearchController.active) {
            self.taskSearchController.active = false
            self.taskSearchController.canResignFirstResponder()
        }
        
        let currentTask : Task = tasks[indexPath.row]
        launchViewStoryLine(currentTask)
    
    }
    
    /**
     Delegate function of the MapView. It is reonsible to add annotations (pins) on the MapView
     */
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MapPin {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
                as? MKPinAnnotationView { // 2
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                // 3
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                let btn = UIButton(type: .DetailDisclosure)
                view.rightCalloutAccessoryView = btn
                //view.rightCalloutAccessoryView = UIButton.buttonWithType(.DetailDisclosure) as UIView
            }
            return view
        }
        return nil
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let view = view.annotation as? MapPin {
            if let taskIndex = view.index {
                let currentTask = tasks[taskIndex]
                self.launchViewStoryLine(currentTask)
            }
        }
    }
    
    func launchViewStoryLine(currentTask : Task) {
        
        let viewController : StoryDetailViewController = (UIStoryboard(name: StoryBoardIdentifiers.MainViewController, bundle: nil) .instantiateViewControllerWithIdentifier(ViewControllerIdentifiers.TaskStoryDetailViewController) as? StoryDetailViewController)!
        
        let currentTask : Task = currentTask
        viewController.title = currentTask.taskName
        viewController.selectedStory = (currentTask.taskName)!
        viewController.storyInterest = InterestType.defaultType + self.username! + "/" + (currentTask.taskName)!
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func getTask() {
        let userData = self.userDataStr.dataUsingEncoding(NSUTF8StringEncoding)
        let userDataJson = JSON(data: userData!)
        
        for (_, subJson) in userDataJson["tasks"] {
            let t = Task()
            if let id = subJson[TaskConstants.TaskId].int {t.taskId = id}
            if let name = subJson[TaskConstants.TaskQuery][TaskConstants.TaskName].string {t.taskName = name
                self.tasksTitleArray.append(name)
            }
            if let createdAt = subJson[TaskConstants.TaskCreatedAt].int {t.taskCreatedAt = createdAt}
            if let running_time = subJson[TaskConstants.TaskRunningTime].int {t.taskRunningTime = running_time}
            if let collectedData = subJson[TaskConstants.TaskCollectedData].int {t.taskCollectedData = collectedData}
            
            if let keyword1 = subJson[TaskConstants.TaskQuery][TaskConstants.Taskkeyword1].string {t.taskQuery.append(keyword1)}
            if let keyword2 = subJson[TaskConstants.TaskQuery][TaskConstants.Taskkeyword2].string {t.taskQuery.append(keyword2)}
            if let keyword3 = subJson[TaskConstants.TaskQuery][TaskConstants.Taskkeyword3].string {t.taskQuery.append(keyword3)}
            
            if let createdBy = subJson[TaskConstants.TaskCreatedBy].string {t.taskCreatedBy = createdBy}
            if let status = subJson[TaskConstants.TaskStatus].string {t.taskStatus = status}
            if let analysisTypes = subJson[TaskConstants.TaskQuery][TaskConstants.TaskAnalysisTypes].arrayObject {
                t.taskAnalysisTypes = analysisTypes as? [String]
            }
            if let mostCommonWords = subJson[TaskConstants.TaskQuery][TaskConstants.TaskMostCommon].arrayObject {
                t.taskMostCommonWords = mostCommonWords as? [String]
            }
            
            if let defaultAnalysis = subJson[TaskConstants.TaskQuery][TaskConstants.TaskDefaultAnalysis].string {t.taskDefaultAnalysis = defaultAnalysis}
            
            if let latitude = subJson[TaskConstants.TaskQuery][TaskConstants.Tasklatitude].string, longitude = subJson[TaskConstants.TaskQuery][TaskConstants.Tasklongitude].string  {
                if(latitude != "" && longitude != "") {
                    t.taskLocation = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
                }
            }
            
            if let location = t.taskLocation, title = t.taskName {
                let mapPin = MapPin(coordinate: location, title : title, index: self.tasks.count)
                self.mapView.addAnnotation(mapPin)
            }
            
            self.tasks.append(t)
        }
        self.tasksTableView.reloadData()
    }
    
    func onUserData(i: Interest, d: Data) {
        let rawContent = d.getContent()
        let userData = NSString(bytes: rawContent, length: rawContent.count, encoding: NSUTF8StringEncoding)
        let userDataStr = String(userData!)
        self.userDataStr += userDataStr
        
        if (d.getContentType() == 9) {
            getTask()
        } else {
            userInfoConsumer.getNext()
        }
    }
    
    func onOpen() {
        print("Info: NDN Open.")
    }
    
    func onClose() {
        print("Info: NDN Close.")
    }
    
    func onError(reason: String) {
        print("Error: NDN - " + reason)
    }
    
    
}

extension TasksViewController: UISearchResultsUpdating
{
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        self.searchArray.removeAll(keepCapacity: false)
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (self.tasksTitleArray as NSArray).filteredArrayUsingPredicate(searchPredicate)
        self.searchArray = array as! [String]
        
        if !searchController.active {
            print ("Cancel clicked!")
            
            searchController.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            //            searchController.willMoveToParentViewController(self)
        }
    }
}
