//
//  InfoMaxConsumer.swift
//  SwiftNDN
//
//  Created by Akash Kapoor on 4/15/15.
//  Copyright (c) 2015 CyPhy UIUC. All rights reserved.
//

import Foundation
import SwiftNDN

public protocol UserInfoConsumerDelegate: class {
    func onOpen()
    func onClose()
    func onUserData(i: Interest, d: Data)
    func onError(reason: String)
}

public class UserInfoConsumer: FaceDelegate {
    
    private var face: Face!
    private var interestPrefix: String
    private var interestPrefixName: Name
    
    weak var delegate: UserInfoConsumerDelegate!
    private var segmentNum: Int!
    
    public init(delegate: UserInfoConsumerDelegate, prefix: String, forwarderIP: String, forwarderPort: UInt16) {
        
        self.delegate = delegate
        self.interestPrefix = prefix
        self.interestPrefixName = Name(url: interestPrefix)!
        self.segmentNum = 1
        
        face = Face(delegate: self, host: forwarderIP, port: forwarderPort)
        face.open()
    }
    
    public func close() {
        face.close()
    }
    
    private func expressInterest(interestPrefix: String, segmentNum: Int) {
        let interest = Interest()
        let interestName = interestPrefix+"/"+String(segmentNum)
        
        print("Info: UserInfoConsumer expressing IM Interest: " + interestName)
        interest.name = Name(url: interestName)!
        interest.setInterestLifetime(1000)
        interest.setMustBeFresh()
        self.face.expressInterest(interest, onData: { [unowned self] in self.onUserData($0, d0: $1) }, onTimeout: { [unowned self] in self.onTimeout($0) })
    }
    
    public func getNext() {
        expressInterest(interestPrefix, segmentNum: segmentNum)
        segmentNum = segmentNum + 1
    }
    
    private func onUserData(i0: Interest, d0: Data) {
        print("Info: UserInfoConsumer - received UserData")
        delegate.onUserData(i0, d: d0)
    }
    
    private func onTimeout(i0: Interest) {
        print("Error: Timeout for " + i0.name.toUri())
    }
    
    // Face Delegate Functions
    public func onOpen() {
        print("Info: UserInfoConsumer Open for prefix " + interestPrefix + ".")
        getNext()
    }
    
    public func onClose() {
        print("Info: UserInfoConsumer Close.")
    }
    
    public func onError(reason: String) {
        print("ERROR: UserInfoConsumer - " + reason)
    }
    
}