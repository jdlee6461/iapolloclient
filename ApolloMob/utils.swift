//
//  File.swift
//  ApolloMob
//
//  Created by panindra Ts on 9/12/15.
//  Copyright © 2015 panindra Ts. All rights reserved.
//

import Foundation
import SystemConfiguration
import SwiftyJSON
import Alamofire
import AlamofireImage

func connectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(&zeroAddress, {
        SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
    }) else {
        return false
    }
    
    var flags : SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.Reachable)
    let needsConnection = flags.contains(.ConnectionRequired)
    return (isReachable && !needsConnection)
}

public struct StoryBoardIdentifiers {
    static let MainViewController = "Main"
    static let TaskDetailsStoryLineViewController = "TaskDetailsStoryLine"
}

public struct ViewControllerIdentifiers {
    static let TasksViewControllerIdentifier = "tasksViewController"
    static let TasksNavigationController = "tasksNavigationController"
    static let TaskTableViewController = "taskTableViewController"
    static let TaskCreateViewController = "createTaskViewController"
    static let TaskImageStoryLineController = "taskImageStoryLineController"
    static let TaskDetailImageStoryLineController = "TaskDetailImageStoryLineController"
    static let TaskFullImageStoryLineController = "taskFullImageStoryLineController"
    static let TaskDetailStoryLineController =  "taskDetailsStoryLineController"
    static let TaskStoryDetailViewController = "taskStoryDetailViewController"
    static let TaskStorySortViewController = "taskStorySortViewController"
    static let TaskStoryVotingViewController = "taskStoryVotingViewController"
    static let TaskStoryDetailSuperViewController = "taskStoryDetailSuperViewController"
}

public struct TaskConstants {
    static let TaskId = "id"
    static let TaskName = "dataset"
    static let TaskCreatedAt = "start_time"
    static let TaskRunningTime = "tweet_count"
    static let TaskCollectedData = "tweet_count"
    static let TaskQuery = "query"
    static let Taskkeyword1 = "keyword1"
    static let Taskkeyword2 = "keyword2"
    static let Taskkeyword3 = "keyword3"
    static let TaskRadius = "radius"
    static let TaskCrawler = "crawler"
    static let TaskCreatedBy = "created_by"
    static let TaskQueryType = "query_type"
    static let TaskStatus = "status"
    static let TaskAnalysisTypes = "analysis_types"
    static let TaskDefaultAnalysis = "default_analysis"
    static let TaskPermission = "permission"
    static let Tasklatitude = "latitude"
    static let Tasklongitude = "longitude"
    static let TaskMostCommon = "mostCommon"
}

public struct TableCellIdentifiers {
    static let TaskDetailsIdentifier = "taskDetailsCell"
    static let TaskCellIdentifier = "taskCell"
    static let TaskImageStorylineCellIdentifier = "taskImageStorylineCell"
}

public struct ApolloBackeEndConstants {
    static let TaskID = "task_id"
    static let DatasetID = "dataset_id"
    static let AnalysisType = "analysis_type"
    static let Capacity = "capacity"
    static let Fresh = "fresh"
}

public struct AlertActionConstants {
    static let AlertActionTitle = "Task Action"
    static let Delete = "Delete"
    static let Pause = "Pause"
    static let Resume = "Resume"
    static let ViewStoryline = "View Storyline"
    static let Cancel = "Cancel"
    static let OK = "OK"
}

public struct TaskStatus {
    static let Active = "active"
    static let Paused = "paused"
    static let Resume = "resume"
    static let Delete = "delete"
}

//let BaseUrl: String = "http://apollo3.cs.illinois.edu/now-bin/now.py/"
let BaseUrl: String = "http://apollo2.cs.illinois.edu/now-bin/now.py/"
let AnalysisUrl: String = "http://apollo2.cs.illinois.edu/analysis-bin/view/frontend.py/"

let OnGoogleSignIn = "OnGoogleSignIn"
let noOfFieldsPerTask : Int = 7


public struct InterestType {
    static let defaultType = "/ndn/edu/umich/Apollo/"
    static let userType = "/ndn/edu/umich/Apollo/user/"
    static let createType = "/ndn/edu/umich/Apollo/create/"
}

public struct InterestTagType {
    static let InfoMaxDiversityTag = "/InfoMax/"
    static let InfoMaxSortTag = "/InfoMaxSort/"
    static let InfoMaxVotingTag = "/InfoMaxVoting/"
    static let InfoMaxNearestNeighborTag = "/InfoMaxNeighbor/"
    static let InfoMaxDivider = "#"
}

public struct viewType {
    static let viewDiversity = 0
    static let viewSort = 1
    static let viewVoting = 2
}

let defaultPipeline : Int = 5
let defaultForwarderIP = "72.36.112.82"
let defaultPort : UInt16 = 6363

func makePostRequest(url: String, parameters: [String: String], completionHandler: (result: NSHTTPURLResponse?, data: NSData?, error: NSError?) -> Void) {
    Alamofire.request(.POST, url, parameters:parameters)
        .response { request, response, data, error in
            completionHandler(result: response, data: data, error: nil)
    }
}

func makeGetRequest(url: String, parameters: [String: String], completionHandler: (result: NSHTTPURLResponse?, data: NSData?, error: NSError?) -> Void) {
    Alamofire.request(.GET, url, parameters:parameters)
        .response { request, response, data, error in
            completionHandler(result: response, data: data, error: nil)
    }
}

func downloadImage(url: String, completionHandler: (result: NSHTTPURLResponse?, image : UIImage) -> Void) {
    Alamofire.request(.GET, url)
        .responseImage { request, response, result in
            if let image = result.value {
                completionHandler(result: response, image: image)
            }
    }
}

func createTask(keyword1 : String, keyword2: String, keyword3 : String, Taskname : String, latitude: String, longitude : String, radius : String, crawler : String, created_by : String, query_type : String, analysis_types : [String], default_analysis : String, permission : String, completionHandler: (NSHTTPURLResponse? -> Void)) {
    
    if Taskname == "" || created_by == "" || default_analysis == "" {
        print("Taskname or Created_by or default analysis fields cannot be empty")
        return
    }
    
    let createUrl: String = BaseUrl + TaskOperations.Create.rawValue
    let parameters: [String: String] = [TaskConstants.Taskkeyword1 : keyword1, TaskConstants.Taskkeyword2 : keyword2, TaskConstants.Taskkeyword3 : keyword3, TaskConstants.TaskName : Taskname, TaskConstants.Tasklatitude : latitude, TaskConstants.Tasklongitude : longitude, TaskConstants.TaskRadius : radius, TaskConstants.TaskCrawler : crawler, TaskConstants.TaskCreatedBy : created_by, TaskConstants.TaskQueryType : query_type, TaskConstants.TaskAnalysisTypes : String(analysis_types), TaskConstants.TaskDefaultAnalysis : default_analysis, TaskConstants.TaskPermission : permission]
    
    makePostRequest(createUrl, parameters: parameters) { (result, data, error) -> Void in
        completionHandler(result)
    }
}


/**
 Utility functon to give the color of the label based on the status of a task
 
 - parameter status: The status of a task based on task action
 
 - returns: The color to be shown for a task
 */
func giveTextColor(status : String)-> UIColor{
    var color = UIColor .grayColor()
    switch status {
    case TaskStatus.Active : color = UIColor.blueColor()
    case TaskStatus.Paused : color = UIColor.orangeColor()
    case TaskStatus.Resume : color = UIColor.greenColor()
    default:
        color = UIColor.grayColor()
    }
    return color
}