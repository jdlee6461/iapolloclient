//
//  ViewController.swift
//  ApolloMob
//
//  Created by panindra Ts on 9/11/15.
//  Copyright © 2015 panindra Ts. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import XCGLogger

struct FacebookConstants {
    static let FBEmail = "email"
    static let FBName = "name"
    static let FBFields = "fields"
}

/// This class represnts the login screen with three options to login.
class LoginViewController: UIViewController, GIDSignInUIDelegate, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var activityController: UIActivityIndicatorView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
    
    var internetPresent = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if connectedToNetwork() {
            internetPresent = true
            usernameTextField.becomeFirstResponder()
            
            /**
            The facebook login delegates and permissions.
            */
//            facebookLoginButton.delegate = self
//            facebookLoginButton.readPermissions = [FacebookConstants.FBEmail]
            
            /**
            Google auto login and delegate settins. The notificaton is received from the Appdelegate when signed in via Google
            */
//            GIDSignIn.sharedInstance().uiDelegate = self
//            GIDSignIn.sharedInstance().signInSilently()
            
//            NSNotificationCenter.defaultCenter().addObserver(self,
//                selector: "receiveGoogleSignNotification:",
//                name: OnGoogleSignIn,
//                object: nil)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if internetPresent == false {
            self.showInternetDisconnectivityMessage()
            return
        }
        /**
        *  The Auto login version using Facebook.
        */
//        if(FBSDKAccessToken.currentAccessToken() != nil) {
//            getFBUserData()
//            let userID = FBSDKAccessToken.currentAccessToken().userID
//            log.info("User is logged in via facebook \(userID)")
//            launchTaskViewController("admin")
//        }
//        else {
//            log.info("User is not logged in")
//        }
    }
    
    func showInternetDisconnectivityMessage() {
        let alert = UIAlertController(title: "No Internet Connection", message: "Please make sure internet connection is present in the device", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    /**
     Function cheks whehter user is signed In. If so launces the tasks view controller using credentials.
     */
    func checkWhetherUserSignedIn(username : String = "admin") {
        if (GIDSignIn.sharedInstance().hasAuthInKeychain()){
            // Signed in
            launchTaskViewController(username)
        }
    }
    
    /**
     Notification received when Signed in via Google.
     
     - parameter notification: Notification
     */
    func receiveGoogleSignNotification(notification : NSNotification) {
        if notification.name == OnGoogleSignIn {
            checkWhetherUserSignedIn()
        }
    }
    
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        activityController.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
        presentViewController viewController: UIViewController!) {
            self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
        dismissViewController viewController: UIViewController!) {
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    /**
     Function to execute manual Login process
     
     - parameter sender: Button
     */
    @IBAction func onLogin(sender: AnyObject) {
        if internetPresent == false {
            self.showInternetDisconnectivityMessage()
            return
        }
        
        if let userName = usernameTextField.text, password = passwordTextField.text {
            activityController.startAnimating()
            
            let loginUrl: String = BaseUrl + "checkCredentials"
            let parameters: [String: String] = ["userName": userName, "password" : password]
            
            makePostRequest(loginUrl, parameters : parameters, completionHandler: { (result, data, error) -> Void in
                if result?.statusCode == 200 {
                    self.activityController.stopAnimating()
                    self.launchTaskViewController(userName)
                }
            })
        }
    }
    
    /**
     Function to launch TasksViewController in case of successful login
     
     - parameter userName: The Username of the signed in user.
     */
    func launchTaskViewController(userName : String) {
        if let tasksNavigationController = UIStoryboard(name: StoryBoardIdentifiers.MainViewController, bundle: nil).instantiateViewControllerWithIdentifier(ViewControllerIdentifiers.TasksNavigationController) as? UINavigationController {
            if let viewController: TasksViewController = tasksNavigationController.topViewController as? TasksViewController {
                viewController.username = "ndn"
//                viewController.username = userName
                self.presentViewController(tasksNavigationController, animated: true, completion: nil)

            }
        }
    }
    
    /**
     Facebook Delegates called when a Login happens
     
     - parameter loginButton: FB Login Button
     - parameter result:      Result of Login process
     - parameter error:       Error if any
     */
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if error == nil && !result.isCancelled {
            launchTaskViewController("admin")
        }
    }
    
    /**
     Another FB Login delegate to implement
     
     - parameter loginButton: FB Login Button
     */
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    /**
     Function to get Signed In details of a user from Facebook
     */
    func getFBUserData(){
        FBSDKGraphRequest(graphPath: "me", parameters: [FacebookConstants.FBFields: "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
            if (error == nil){
                if let userName = result[FacebookConstants.FBName] as? String {
                    log.info(userName)
                }
                
                if let email = result[FacebookConstants.FBEmail] as? String {
                    log.info(email)
                }
            }
        })
    }
}

