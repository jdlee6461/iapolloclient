//
//  InfoMaxConsumer.swift
//  SwiftNDN
//
//  Created by Akash Kapoor on 4/15/15.
//  Copyright (c) 2015 CyPhy UIUC. All rights reserved.
//

import Foundation
import SwiftNDN

public protocol CreateRequesterDelegate: class {
    func onOpen()
    func onClose()
    func onError(reason: String)
}

public class CreateRequester: FaceDelegate {
    
    private var face: Face!
    private var interestPrefix: String
    private var interestPrefixName: Name
    
    weak var delegate: CreateRequesterDelegate!
    
    public init(delegate: CreateRequesterDelegate, prefix: String, forwarderIP: String, forwarderPort: UInt16) {
        
        self.delegate = delegate
        self.interestPrefix = prefix
        self.interestPrefixName = Name(url: interestPrefix)!
        
        face = Face(delegate: self, host: forwarderIP, port: forwarderPort)
        face.open()
    }
    
    public func close() {
        face.close()
    }
    
    private func expressInterest(interestPrefix: String) {
        let interest = Interest()
        let interestName = interestPrefix
        
        print("Info: CreateRequester expressing IM Interest: " + interestName)
        interest.name = Name(url: interestName)!
        interest.setInterestLifetime(1000)
        interest.setMustBeFresh()
        self.face.expressInterest(interest, onData: { [unowned self] in self.onRequestData($0, d0: $1) }, onTimeout: { [unowned self] in self.onTimeout($0) })
    }
    
    public func makeRequest(interest: String) {
        print ("make request for "+interest)
        expressInterest(interest)
    }
    
    private func onRequestData(i0: Interest, d0: Data) {
        print("Info: CreateRequester - received ACK")
    }
    
    private func onTimeout(i0: Interest) {
        print("Error: Timeout for " + i0.name.toUri())
    }
    
    // Face Delegate Functions
    public func onOpen() {
        print("Info: UserInfoConsumer Open for prefix " + interestPrefix + ".")
    }
    
    public func onClose() {
        print("Info: UserInfoConsumer Close.")
    }
    
    public func onError(reason: String) {
        print("ERROR: UserInfoConsumer - " + reason)
    }
    
}