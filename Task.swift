//
//  Task.swift
//  ApolloMob
//
//  Created by panindra Ts on 9/12/15.
//  Copyright © 2015 panindra Ts. All rights reserved.
//

import Foundation
import MapKit

enum TaskOperations :String {
    case Delete = "delete_task", Pause = "pause_task", Resume = "resume_task", Export = "export_task", Archive = "archive_task", Create = "create_task"
}

class Task: NSObject {
    var taskId: Int?
    var taskName: String?
    var taskCreatedAt: Int?
    var taskRunningTime :Int?
    var taskCollectedData: Int?
    var taskQuery : [String] = []
    var taskCreatedBy: String?
    var taskStatus : String?
    var taskAnalysisTypes : [String]?
    var taskDefaultAnalysis : String?
    var taskLocation: CLLocationCoordinate2D?
    var taskMediumTypes : [String]?
    var taskMostCommonWords : [String]?

    override var description : String {
        return "taskId : \(taskId) \n taskName : \(taskName) \n taskCreatedAt :\(taskCreatedAt) \n taskRunningTime : \(taskRunningTime) \n taskCollectedData : \(taskCollectedData) \n taskQuery : \(taskQuery) \n taskCreatedBy : \(taskCreatedBy) \n taskStatus : \(taskStatus) \n taskAnalysisTypes : \(taskAnalysisTypes) \n taskDefaultAnalysis : \(taskDefaultAnalysis) \n taskMostCommonWords : \(taskMostCommonWords)"
    }       
    
    //Delete, Pause, Resume, Export, Archive, 
    func performOperationOnTask(task : TaskOperations, completionHandler: (result: NSHTTPURLResponse?) -> Void) {
        guard let taskId = self.taskId else {
            print("Task Id cannot be null")
            return
        }

        let deleteUrl: String = BaseUrl + task.rawValue
        let parameters: [String: String] = [ApolloBackeEndConstants.TaskID: String(taskId)]
        
        makePostRequest(deleteUrl, parameters: parameters) { (result, data, error) -> Void in
            completionHandler(result: result)
        }
    }
    
    func getImageAndTweets(analysisType : String, capacity : String, fresh : String, compltionHandler : (result : NSHTTPURLResponse?, data : NSData?) -> Void) {
        let feedUrl = AnalysisUrl + "get_feed"
        guard let taskid = self.taskId else { return }
        let parameters: [String: String] = [ApolloBackeEndConstants.DatasetID: String(taskid), ApolloBackeEndConstants.AnalysisType : analysisType, ApolloBackeEndConstants.Capacity : capacity, ApolloBackeEndConstants.Fresh : fresh]
        
        print(parameters)
        makePostRequest(feedUrl, parameters: parameters) { (result, data, error) -> Void in
            compltionHandler(result: result, data: data)
        }
    }
}