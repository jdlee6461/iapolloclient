//
//  InfoMaxConsumer.swift
//  SwiftNDN
//
//  Created by Akash Kapoor on 4/15/15.
//  Copyright (c) 2015 CyPhy UIUC. All rights reserved.
//

import Foundation
//import SwiftNDN

public protocol InfoMaxConsumerDelegate: class {
    func onOpen()
    func onClose()
    func onData(i: Interest, d: Data)
    func onError(reason: String)
    func test()
}

public class InfoMaxConsumer: FaceDelegate {
    
    private var INFOMAX_INTEREST_TAG = ""
    private var INFOMAX_INTEREST_AUX_CHAR: Character!
    private var INFOMAX_NEIGHBOR_INTEREST_TAG = ""

    var elementArray: [String] = []
    var elementIndex: Int = -1
    
    weak var delegate: InfoMaxConsumerDelegate!
    
    private var face: Face!
    private var interestPrefix: String!
    private var interestPrefixName: Name!
    
    public init(delegate: InfoMaxConsumerDelegate, prefix: String, forwarderIP: String, forwarderPort: UInt16) {
        INFOMAX_INTEREST_AUX_CHAR = "#"
        INFOMAX_INTEREST_TAG = "/InfoMax/"
        INFOMAX_NEIGHBOR_INTEREST_TAG = "/InfoMaxNeighbor"
        
        self.delegate = delegate
        
        interestPrefix = prefix
        interestPrefixName = Name(url: self.interestPrefix)
        
        face = Face(delegate: self, host: forwarderIP, port: forwarderPort)
        face.open()
    }
    
    public func close() {
        face.close()
    }
    
    public func get(count: Int){
        expressInfoMaxInterest(count)
    }
    
    public func getNearestNeighbor(suffix: String) {
        expressNearestNeighborInterest(suffix)
    }
    
    private func expressNearestNeighborInterest(suffix: String) {
        let interest = Interest()
        var nameUrl:String!
        nameUrl = self.interestPrefix + self.INFOMAX_NEIGHBOR_INTEREST_TAG + suffix
        print("Info: InfoMaxConsumer expressing *NN* Interest: " + nameUrl)
        interest.name = Name(url: nameUrl)!
        interest.setInterestLifetime(1000)
        interest.setMustBeFresh()
        self.face.expressInterest(interest, onData: { [unowned self] in self.onInfoMaxList($0, d0: $1) }, onTimeout: { [unowned self] in self.onInfoMaxTimeout($0) })
    }
    
    private func expressInfoMaxInterest(count: Int) {
        let interest = Interest()
        var nameUrl:String!
        nameUrl = self.interestPrefix + self.INFOMAX_INTEREST_TAG + String(self.INFOMAX_INTEREST_AUX_CHAR)
        nameUrl = nameUrl + String(count) + String(self.INFOMAX_INTEREST_AUX_CHAR) + "0"
        print("Info: InfoMaxConsumer expressing IM Interest: " + nameUrl)
        interest.name = Name(url: nameUrl)!
        interest.setInterestLifetime(1000)
        interest.setMustBeFresh()
        self.face.expressInterest(interest, onData: { [unowned self] in self.onInfoMaxList($0, d0: $1) }, onTimeout: { [unowned self] in self.onInfoMaxTimeout($0) })
    }
    
    private func onInfoMaxNNList(i0: Interest, d0: Data) {
        print("Info: InfoMaxNNConsumer - received InfoMaxNNList")
    }
    
    private func onInfoMaxList(i0: Interest, d0: Data) {
        print("Info: InfoMaxConsumer - received InfoMaxList")
        let a = d0.getContent()
        let list = NSString(bytes: a, length: a.count, encoding: NSUTF8StringEncoding)
        let elements = list!.componentsSeparatedByString(" ")
        print("Info: List- ")
        print(elements)
        
        elementArray.removeAll(keepCapacity: true)
        
        for element in (elements as [String]) {
            if element.characters.count > 3 {
                elementArray.append(element)
            }
        }
        if elementArray.count > 0 {
            elementIndex = 0
        }
        getNextElement()
    }
    
    private func getNextElement() {
        print("in get Nex" + String(elementIndex) + String(elementArray.count))
        if (elementIndex > -1 && elementIndex < elementArray.count) {
            print("Over here")
            let interest = Interest()
            let element = elementArray[elementIndex]
            print("Info: InfoMaxConsumer expressing Interest: " + element)
            interest.name = Name(url: interestPrefix+element)!
            interest.setInterestLifetime(1000)
            interest.setMustBeFresh()
            self.face.expressInterest(interest, onData: { [unowned self] in self.onElement($0, d0: $1) }, onTimeout: { [unowned self] in self.onElementTimeout($0) })
            elementIndex = elementIndex + 1
        }
    }
    
    private func onInfoMaxTimeout(i0: Interest) {
        print("Error: InfoMaxConsumer - timeout for " + i0.name.toUri())
    }
    
    private func onElement(i0: Interest, d0: Data) {
        print("Info: Received element - " + i0.name.toUri())
        let applicationInterestName = Name()
        
        for index in interestPrefixName.size...i0.name.size {
            if let component = i0.name.getComponentByIndex(index) {
                applicationInterestName.appendComponent(component)
            }
        }
        
        i0.name = applicationInterestName
        delegate.test()
        delegate.onData(i0, d: d0)
        getNextElement()
    }
    
    private func onElementTimeout(i0: Interest) {
        print("Error: Timeout for " + i0.name.toUri())
    }
        
    // Face Delegate Functions
    public func onOpen() {
        print("Info: InfoMaxConsumer Open for prefix " + interestPrefix + ".")
        delegate.onOpen()
    }
    
    public func onClose() {
        print("Info: InfoMaxConsumer Close.")
        delegate.onClose()
    }
    
    public func onError(reason: String) {
        print("ERROR: InfoMaxConsumer - " + reason)
    }

}