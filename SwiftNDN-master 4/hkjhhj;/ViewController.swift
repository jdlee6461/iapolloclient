//
//  ViewController.swift
//  hkjhhj;
//
//  Created by Akash Kapoor on 4/12/15.
//  Copyright (c) 2015 Wentao Shang. All rights reserved.
//

import Cocoa
import SwiftNDN

class ViewController: NSViewController, InfoMaxConsumerDelegate {

    var consumer: InfoMaxConsumer!
    var prefix: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        dispatch_async(dispatch_get_main_queue()) {
            self.prefix = "/test" //"/ndn/apollo/stories"
            self.consumer = InfoMaxConsumer(delegate: self, prefix: self.prefix, forwarderIP: "ndnx.cs.illinois.edu", forwarderPort: 6363)
        }
        
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    func onData(d: Data) {
        var a = d.getContent()
        var list = NSString(bytes: a, length: a.count, encoding: NSUTF8StringEncoding)
        println("Data: ")
        print(list)
    }
    
    func onOpen() {
        self.consumer.get(1)
    }
    
    func onError(reason: String) {
    }
    
    func onClose() {
    }
}