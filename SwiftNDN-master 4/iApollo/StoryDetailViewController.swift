//
//  StoryDetailViewController.swift
//  InfoMaxApollo
//
//  Copyright (c) 2015 akash_kapoor. All rights reserved.
//

import UIKit
//import SwiftNDN

class StoryDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, InfoMaxConsumerDelegate {
    @IBOutlet weak var typeNavigator: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var arrayOfTweets:[Tweet] = [Tweet]()
    var selectedStory = ""
    var storyInterest = ""
    var showPopUp = false
    var consumer: InfoMaxConsumer!
    var prefix: String!
    
    var getCount = 1
    var timer = NSTimer()

    @IBAction func typeChanged(sender: UISegmentedControl) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: "backButton:")
        let nextBut = UIBarButtonItem(title: "More", style: UIBarButtonItemStyle.Plain, target: self, action: "buttonAction:" )
        self.navigationItem.setRightBarButtonItem(nextBut, animated: true)
        self.navigationItem.setLeftBarButtonItem(backButton, animated: true)

        // Do any additional setup after loading the view.
        UINavigationBar.appearance().backgroundColor = UIColor.lightGrayColor()
        self.automaticallyAdjustsScrollViewInsets = false

        openInfoMaxConnection()
        
    }
    
    func test()
    {
        print ("test")
    }

    func openInfoMaxConnection()
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        self.prefix = self.storyInterest
        self.consumer = InfoMaxConsumer(delegate: self, prefix: self.prefix, forwarderIP: "72.36.112.82", forwarderPort: 6363)
        }
    }
    
    func backButton(sender:UIButton!)
    {
        if(showPopUp == true){
            print ("back button clicked")
            showPopUp = false
            UIApplication.sharedApplication().openURL(NSURL(string: "https://docs.google.com/forms/d/1xrGdiBCIpGPTz6hi4HlCDuP3nMLWPh3ULKFDSKmJXIs/viewform?c=0&w=1&usp=mail_form_link")!)
            
//            let alert = UIAlertController(title: "How relevant were the tweets?", message: "Message", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction(title: "1. Very relevant", style: UIAlertActionStyle.Default, handler: nil))
//            alert.addAction(UIAlertAction(title: "2. Relevantt", style: UIAlertActionStyle.Default, handler: nil))
//            alert.addAction(UIAlertAction(title: "3. Somewhat relevant", style: UIAlertActionStyle.Default, handler: nil))
//            alert.addAction(UIAlertAction(title: "4. Not at all", style: UIAlertActionStyle.Default, handler: nil))
//
//            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func buttonAction(sender:UIButton!)
    {
        getNextTweetSet()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: InfoMaxDelegate
    
    func onData(i: Interest, d: Data) {
        
        let rawContent = d.getContent()
        let tweetText = NSString(bytes: rawContent, length: rawContent.count, encoding: NSUTF8StringEncoding)
        let tweetStr = String(tweetText!)
        
        print("interest: "+i.name.toUri())
        print("data: "+tweetStr)
        
        let JSONData = tweetStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        
        var tweetDict = [:]
        do {
            tweetDict = try NSJSONSerialization.JSONObjectWithData(JSONData!, options: .AllowFragments) as! NSDictionary
        } catch {
            print(error)
        }
        
        if(tweetDict.count == 0) {
            return
        }
        
        let twe = Tweet(userName: " ", tweet: tweetDict.valueForKey("text") as! String, time: tweetDict.valueForKey("created_at") as! String, imageName: "q.png", suffix:i.name.toUri())

        arrayOfTweets.append(twe)
        
        let fb = NSIndexPath(forRow: self.arrayOfTweets.count-1, inSection: 0)
        self.tableView.insertRowsAtIndexPaths([fb], withRowAnimation: UITableViewRowAnimation.Automatic)
    }
    
    func getNextTweetSet() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.consumer.get(self.getCount)
            self.getCount = self.getCount + 1
        }
    }
    
    func onOpen() {
        getNextTweetSet()
        timer = NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: "timeElapsed", userInfo: nil, repeats: false)
    }
    
    func timeElapsed(){
        print("YOLO")
        showPopUp = true
        timer.invalidate()
    }
    
    func onError(reason: String) {
    }
    
    func onClose() {
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrayOfTweets.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as? CustomCell

        let tweet = arrayOfTweets[indexPath.row]
        cell!.setCell(tweet.userName, tweet: tweet.tweet, time: tweet.time, imageName: tweet.imageName)
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let indexPath = self.tableView.indexPathForSelectedRow()?.row
        //var selectedTweet = self.arrayOfTweets[indexPath!]
        //self.consumer.getNearestNeighbor(selectedTweet.suffix)
        //self.performSegueWithIdentifier("SuperStoryDetailSegue", sender: tableView)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "SuperStoryDetailSegue" {
            if let destinationVC = segue.destinationViewController as? SuperStoryDetail{
                let indexPath = self.tableView.indexPathForSelectedRow?.row
                let selectedTweet = self.arrayOfTweets[indexPath!]
                print(selectedTweet.tweet)
                destinationVC.title = selectedTweet.tweet
                destinationVC.prefix = self.prefix
                destinationVC.selectedStorySuffix = selectedTweet.suffix
            }
        }
    }}
